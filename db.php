<?php

require_once 'dbconfiq.php';

try {

    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $latitude = filter_input(INPUT_POST, 'latitude', FILTER_SANITIZE_STRING);
    $longitude = filter_input(INPUT_POST, 'longitude', FILTER_SANITIZE_STRING);
    $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    $stmt = $conn->prepare("INSERT INTO maps (title, latitude, longitude, description) 
    VALUES (:title, :latitude, :longitude, :description)");
    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':latitude', $latitude);
    $stmt->bindParam(':longitude', $longitude);
    $stmt->bindParam(':description', $description);
    $stmt->execute();

    header("Location: https://maps.sandrapoll.com");

} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
