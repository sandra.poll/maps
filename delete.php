<?php

require_once 'dbconfiq.php';

try {

    $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    $stmt = $conn->prepare("DELETE FROM maps WHERE id = :id");
    $stmt->bindParam('id', $id);
    $stmt->execute();

    header("Location: https://maps.sandrapoll.com/");

} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
