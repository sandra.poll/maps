<?php

require_once 'dbconfiq.php';

try {

    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $stmt = $conn->query("SELECT * FROM maps");

    $data = [];

    while ($row = $stmt->fetch()) {
        $data[] = [
            'id' => $row['id'],
            'title' => $row['title'],
            'latitude' => $row['latitude'],
            'longitude' => $row['longitude'],
            'description' => $row['description'],
        ];
    }

    $data = json_encode($data);
    echo $data;


} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;