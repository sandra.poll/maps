Url - https://maps.sandrapoll.com/

On this website, you can save places to database and see them on the page. To save a place you need to enter latitude, longitude, a title and a description.
Deleting a place from the list is also possible.

This website uses Maps API https://developers.google.com/maps/documentation.
