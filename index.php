<!DOCTYPE html>
<html>
<head>
    <title>Maps</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 600px;
        }

        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        .btn {
            margin-top: 10px;
        }

        table {
            border: 1px solid #b2dba1;
            margin: 15px;
        }

        td {
            border-right: 1px solid #b2dba1;
            padding: 5px;
        }

        tr {
            border-bottom: 1px solid #b2dba1;
        }

    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-xs-12">
            <div id="map"></div>
        </div>
        <div class="col-md-4 col-xs-12">
            <h3 class="display-4">Add Marker</h3>
            <form action="db.php" method="post">
                <label>Title</label>
                <input class="form-control" id="title" name="title" type="text" required>
                <label>Latitude</label>
                <input class="form-control" id="latitude" name="latitude" type="text" required>
                <label>Longitude</label>
                <input class="form-control" id="longitude" name="longitude" type="text" required>
                <label>Description</label>
                <textarea name="description" id="description" class="form-control" type="text" required></textarea>
                <input type="submit" value="Send" id="send" class="btn btn-primary">
            </form>
        </div>
        <table id="table">

        </table>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

    function initMap() {
        let map;

        $.getJSON('https://maps.sandrapoll.com/data.php', function (result) {

            $.each(result, function (i, field) {

                $("#table").append(
                    "<tr><td>" + field.title + "</td><td>" + field.latitude + "</td><td>" + field.longitude +
                    "</td><td>" + field.description + "</td><td style='color: red;cursor: pointer;' id='remove'>" +
                    "<a href='delete.php?id=" + field.id + "'> <button type='submit' class='btn btn-primary'>Delete</button>" +
                    "</a></td></tr>");
                $("#table").append(result);

                let marker = new google.maps.Marker({
                    position: {lat: Number(field.latitude), lng: Number(field.longitude)},
                    map: map,
                    animation: google.maps.Animation.DROP
                });

                let content = '<div id="content">' +
                    '<div id="info">' +
                    '</p><p>Title: </p><p>' + field.title +
                    '</p><p>Latitude: </p>' +
                    '<i>' + field.latitude + '</i>' +
                    '<p>Logitude: </p>' +
                    '<i>' + field.longitude + '</i>' +
                    '<p>Description: </p><p>' + field.description +
                    "</p><a href='delete.php?id=" + field.id + "'> <button type='submit' class='btn btn-primary'>Delete</button></a></div>";

                let infoWindow = new google.maps.InfoWindow({
                    content: content
                });

                marker.addListener('click', function () {
                    infoWindow.open(map, marker);
                });

            });
        });


        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 58.2565, lng: 22.4829},
            zoom: 10
        });

        map.addListener('click', function (e) {

            let marker = new google.maps.Marker({
                position: {lat: e.latLng.lat(), lng: e.latLng.lng()},
                map: map,
                animation: google.maps.Animation.DROP
            });

            $("#latitude").val(e.latLng.lat());
            $("#longitude").val(e.latLng.lng());

        });
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4nA0oRUB6kqQrrooItchfwEaVZRrFAOs&callback=initMap"
        async defer></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

</body>
</html>
